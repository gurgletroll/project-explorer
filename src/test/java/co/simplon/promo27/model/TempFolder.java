package co.simplon.promo27.model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Classe utilisée uniquement pour les tests pour générer et remettre à zéro des fichiers
 * et dossiers temporaires pour pouvoir tester l'Explorer et l'Element dans un contexte
 * reproductible et sans toucher aux fichiers de notre machine
 */
public class TempFolder {
    public static Path temp = null;
    /**
     * La structure de fichier à générer, pour les tests, on peut la mettre à jour
     * mais alors il faudra répercuter les changements dans les tests
     */
    public static void generate() {
        try {
            temp = Files.createTempDirectory(null);
            Files.createDirectories(temp.resolve("testfolder"));
            Files.createFile(temp.resolve("testfile"));
            Files.createFile(temp.resolve("testfolder").resolve("testfileinfolder"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Supprime les fichiers temporaires
     */
    public static void cleanUp() {
        if(temp != null) {
            temp.toFile().delete();
        }
    }
}
