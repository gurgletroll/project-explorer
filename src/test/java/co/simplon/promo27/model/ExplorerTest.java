package co.simplon.promo27.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ExplorerTest {
    Explorer instance;

    @BeforeEach
    public void setUp() {
        TempFolder.generate();
        instance = Explorer.getInstance();
        instance.setCurrentLocation(TempFolder.temp);
        
    }

    @AfterEach
    public void reset() {
        TempFolder.cleanUp();
    }


    @Test
    public void shouldInstantiateOnce() {
        assertNotNull(instance);
        assertSame(instance, Explorer.getInstance());

    }

    @Test
    public void shouldBeInitializeInFirstLocation() {
        List<Element> content =  instance.getContent();
        assertEquals(2, content.size());
        assertEquals("testfolder", content.get(0).getName());
    }

    @Test
    public void shouldChangeLocation() {
        instance.changeLocation("testfolder");
        List<Element> content =  instance.getContent();
        assertEquals(1, content.size());
        assertEquals("testfileinfolder", content.get(0).getName());
    }

    @Test
    public void copiedShouldPaste() {
        instance.setCopied(instance.getContent().get(1));
        instance.changeLocation("testfolder");
        instance.paste();
        assertEquals(2, instance.getContent().size());
        assertEquals("testfile", instance.getContent().get(0).getName());
    }

    @Test
    public void noCopiedShouldNotPaste() {
        //TODO: test si on paste sans avoir copié, rien ne se passe
    }

    @Test
    public void shouldCreateFile() {
        //TODO: test de création de fichier
    }

    @Test
    public void shouldNotCreateFileIfAlreadyExists() {
        //TODO: test de création de fichier déjà existant
    }

}
