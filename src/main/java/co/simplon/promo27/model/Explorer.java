package co.simplon.promo27.model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Classe servant à naviguer dans les dossiers, lister et manipuler leur
 * contenu.
 * Elle représente en quelque sorte l'état actuel de la navigation.
 * Cette classe a été faite sous forme de Singleton afin qu'on ne puisse
 * l'instancier qu'une seule fois
 * et que tous les composant l'utilisant partagent la même instance/le même état
 */
public class Explorer {
    private static Explorer instance;

    /**
     * Le chemin actuel de la navigation, dans quel dossier se trouve-t-on. Par
     * défaut le dossier de lancement du programme
     */
    private Path currentLocation = Path.of(".").toAbsolutePath();
    /**
     * La liste des fichiers et dossiers dans l'emplacement actuel, mise à jour à
     * chaque déplacement
     */
    private List<Element> content = new ArrayList<>();
    /**
     * L'élément actuellement copié, s'il y en a un
     */
    private Element copied = null;

    /**
     * Méthode à appeler pour obtenir une instance de l'Explorer
     * 
     * @return l'instance de l'explorer
     */
    public static Explorer getInstance() {
        if (instance == null) {
            instance = new Explorer();
        }
        return instance;
    }

    public List<Element> getContent() {
        return content;
    }

    /**
     * constructeur privé pour les besoins du Singleton
     */
    private Explorer() {
        listContent();
    }

    /**
     * Permet de naviguer dans un autre dossier relatif au dossier actuel. Met à
     * jour le content.
     * 
     * @param folder le nom du dossier dans lequel déplacer l'explorer
     */
    public void changeLocation(String folder) {
        if (Files.exists(currentLocation.resolve(folder)) && Files.isDirectory(currentLocation.resolve(folder))) {
            currentLocation = currentLocation.resolve(folder);
            listContent();
        }
    }

    /**
     * Crée un nouveau fichier à l'emplacement actuel de l'explorer
     * 
     * @param name Le nom du nouveau fichier
     * @throws IOException Exception levé si nom de fichier déjà existant ou si pas
     *                     les droits
     */
    public void createFile(String name) throws IOException {
        Files.createFile(currentLocation.resolve(name));
        listContent();
    }

    /**
     * Colle l'élément actuellement "copié" à l'emplacement actuel s'il y a un
     * élément copié.
     */
    public void paste() {
        if (copied != null) {
            try {
                Files.copy(copied.getFile().toPath(), currentLocation.resolve(copied.getName()));
                copied = null;
                listContent();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Liste le contenu de l'emplacement actuel sous forme d'instance de la classe
     * Element
     */
    private void listContent() {
        try {
            content.clear();
            Stream<Path> paths = Files.list(currentLocation);
            for (Path element : paths.toList()) {
                content.add(new Element(element.toFile()));
            }
            paths.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Element getCopied() {
        return copied;
    }

    /**
     * Permet d'assigner un élément à copier
     * 
     * @param copied L'élément à copier
     */
    public void setCopied(Element copied) {
        this.copied = copied;
    }

    public String getCurrentLocation() {
        return currentLocation.toString();
    }

    /**
     * Permet de changer arbitrairement de dossier sans passer par le chemin actuel.
     * Utilisé dans le tests uniquement.
     * 
     * @param currentLocation Le nouvel emplacement où positionner le terminal
     */
    void setCurrentLocation(Path currentLocation) {
        if (Files.exists(currentLocation) && Files.isDirectory(currentLocation)) {
            this.currentLocation = currentLocation;
            listContent();
        }
    }

}
