package co.simplon.promo27.component;

import java.io.IOException;

import co.simplon.promo27.App;
import co.simplon.promo27.model.Element;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * Composant externalisé représentant un fichier ou un dossier dans l'affichage.
 * 
 */
public class FileDisplay extends VBox {
    /**
     * L'élément représenté par ce composant
     */
    private Element element;

    @FXML
    private Label filename;

    public FileDisplay(Element element) {
        this.element = element;
        //La vue se trouve avec les autres dans src/main/resources/co/simplon/promo27
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("component/filedisplay.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        

        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void initialize() {
        filename.setText(element.getName());
    }
}
